public class Main {
	public static void main(String[] args){
		String strUrl = "http://w3c.org/";
		String host   = "127.0.0.1";
		//String host = "78.109.137.225";
		int port = 8080;
		
		ProxyClient p = new ProxyClient(strUrl, host, port);
		Thread thread = new Thread(p);
		thread.start();
	}
}
