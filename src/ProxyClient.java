import java.io.*;
import java.net.*;
public class ProxyClient implements Runnable{
	private    int m_port = 0;
	private String m_host = null;
	private String m_strUrl;
	ProxyClient(String addres, String host, int port){
		m_strUrl = addres;
		m_host = host;
		m_port = port;
	}
	public void run(){
		try{
			System.out.println("HOST: " + m_host);
			System.out.println("PORT: " + m_port);
			System.out.println(" URL: " + m_strUrl + "\n");   
			
		    URL url = null;
		    try {
				 url = new URL(m_strUrl);
			} 
		    catch (MalformedURLException e){
				System.err.println("<URL> is incorrect\n"+e.toString());
				return;
			}
		    
		    //устанавливаем соединение
		    Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(m_host, m_port));
		    HttpURLConnection uc = (HttpURLConnection)url.openConnection(proxy);
		    
		    int respCode = uc.getResponseCode();
		    if(respCode == -1 || respCode == 400){
		    	System.err.println("\nServer returned HTTP response code: " + respCode);
		    	return;
		    }
		    String line = null;
		    String page = null;
		    BufferedReader in =null;
		    if (uc.getContentLength() > 0 || uc.getContentLength()==-1) 
		    	in= new BufferedReader(new InputStreamReader(uc.getInputStream()));
		    while ((line = in.readLine()) != null){
		       page += (line+"\n");		    
		    }
		    System.out.println(page);		    
		}
		catch (IOException e){
			e.printStackTrace();
			
		}
	}	
}

